# LaTeX Template for Homework

![build](/../badges/master/build.svg)

## Sur la compilation du projet

La dernière version du PDF sur `master` est disponible [ici](/../builds/artifacts/master/raw/main.pdf?job=building-latex).

Celle-ci est mise à jour à chaque version nouvelle fusion dans `master`.
