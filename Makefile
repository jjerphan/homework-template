TARGET=main.pdf

PDFLATEX=pdflatex -shell-escape -halt-on-error

main.pdf: main.tex commands.tex maths.tex envs.tex content.tex settings.tex
	$(PDFLATEX) main.tex
	$(PDFLATEX) main.tex


.SECONDARY: $(GENERATED)

.PHONY: clean mrproper

clean:
	rm -f \
		$(TARGET:%.pdf=%.aux) \
		$(TARGET:%.pdf=%.out) \
		$(TARGET:%.pdf=%.tdo) \
		$(TARGET:%.pdf=%.toc) \
		$(TARGET:%.pdf=%.log) \
		$(TARGET:%.pdf=%.bbl) \
		$(TARGET:%.pdf=%.blg) \
		$(TARGET:%.pdf=%.gls) \
		$(TARGET:%.pdf=%.ist) \
		$(TARGET:%.pdf=%.glg) \
	rm -rf generated/


mrproper: clean
	rm -f \
		$(TARGET) \

