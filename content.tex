% Here you go!
\section*{Exercise 1: LP Duality}

For given $c \in \mathbb{R}^d$, $b \in \mathbb{R}^n$ and $A \in \mathbb{R}^{n \times d}$ consider the two following linear optimization problems:

\begin{equation}
    \label{P}
    \begin{matrix}
        & \min_{x} c^\top x \\
        \text{s.t} & Ax = b \\
        & x \geq 0
    \end{matrix}
\end{equation}

and

\begin{equation}
    \label{D}
    \begin{matrix}
        & \max_{y} b^\top y \\
        \text{s.t} & A^\top y \leq c
    \end{matrix}
\end{equation}

\linehorizon

1. Compute the dual of problem \eqref{P} and simplify it if possible.

\linehorizon

We have the dual function:
$$
g: (\lambda, \mu) \mapsto \inf_x \left\{ c^\top x + \lambda^\top (Ax -b) - \mu^\top x\right\}
$$

We compute the Lagrangian of this function. For $\lambda \in (\mathbb{R}^+)^n$, $\mu \in \mathbb{R}^d$:

$$
\mathcal{L}(x, \lambda, \mu) = c^\top x + \lambda^\top (Ax - b) - \mu^\top x = (A^\top \lambda + c - \mu)^\top x - \lambda^\top b
$$

$\mathcal{L}$ is affine in $x$, hence is differential in $x$ and we have:

$$
\nabla_x \mathcal{L}(x, \lambda, \mu) = (A^\top \lambda + c - \mu)
$$

as $\mathcal{L}$ is also convex, we have by the first condition on optimality:

$$
A^\top \lambda + c - \mu = 0
$$

hence we have

$$
g(\lambda, \mu) = -b^\top \lambda
$$

Thus we obtain the following dual problem:

\begin{equation}
    \label{bidule}
    \begin{matrix}
        & \min_\lambda b^\top \lambda \\
        \text{s.t} & A^\top \lambda + c - \mu = 0 \\
        &\lambda \succeq 0
    \end{matrix}
\end{equation}


\linehorizon

2. Compute the dual of problem \eqref{D}.

\linehorizon

We have the following Lagrangian:
$$
\mathcal{L}(y, \mu) = (A \mu - b)^\top y - c^\top\mu
$$

$\mathcal{L}$ is affine in $y$, hence is differential in $y$ and we have:

$$
\nabla_y \mathcal{L}(y, \mu) = (A \mu - b)
$$

By the first conditions on optimality, we get :

$$
g(\lambda, \mu) = - c^\top \mu \quad \text{and} \quad A^\top \mu = b
$$

Hence, the following dual problem:

\begin{equation}
    \begin{matrix}
        & \min_{\mu} \mu^\top c \\
        \text{s.t} & A^\top \mu = b
    \end{matrix}
\end{equation}


\linehorizon

3. A problem is called \emph{self-dual} if its dual is the problem itself. Prove that the following problem is self-dual.

\begin{equation}
    \label{self-dual}
    \begin{matrix}
        & \min_{x, y} c^\top x - b^\top y \\
        \text{s.t} & A x = b \\
        & x \geq 0 \\
        & A^\top y \leq c
    \end{matrix}
\end{equation}

\linehorizon

We have the following Lagrangian, $\mu \succeq 0$:

$$
\mathcal{L}(x, y, \xi, \lambda, \mu) = c^\top x - b^\top y + \lambda^\top (Ax -b) - \mu^\top x + \xi^\top(A^\top y - c)
$$

that is:

$$
\mathcal{L}(x, y, \xi, \lambda, \mu) = (c + A^\top \lambda - \mu)^\top x + (A \xi - b)^\top y - \lambda^\top b - \xi^\top c
$$

$\mathcal{L}$ is linear in $x$ and in $y$, thus $\mathcal{L}$ is differentiable on those variables and we have:


$$
\frac{\partial \mathcal{L}}{\partial x} (x, y, \xi, \lambda, \mu) = c + A^\top \lambda - \mu
$$

$$
\frac{\partial \mathcal{L}}{\partial y} (x, y, \xi, \lambda, \mu) = A \xi - b
$$

$\mathcal{L}$ is also convex (as it is linear), hence by the first conditions on optimality, we have:

\begin{equation}
    \label{tamere}
    \left\{
    \begin{matrix}
        c + A^\top \lambda &=& \mu \\
        A \xi &=& b
    \end{matrix}
    \right.
\end{equation}

We thus have:

$$
g(\xi, \lambda) = - c^\top \xi - \lambda^\top b
$$

By rearranging the conditions \eqref{tamere}, and as $\mu > 0$ we get the following ones:

\begin{equation}
    \label{tamere}
    \left\{
    \begin{matrix}
        A \xi &=& b \\
        \lambda &\preceq& 0 \\
        A^\top \lambda &\preceq& c
    \end{matrix}
    \right.
\end{equation}


We get the following problem:

\begin{equation}
    \begin{matrix}
        & \min_{\xi, \lambda} c^\top \xi - b^\top \lambda \\
        \text{s.t} & \left\{
        \begin{matrix}
            A \xi &=& b \\
            \lambda &\preceq& 0 \\
            A^\top \lambda &\preceq& c
        \end{matrix} \right.
    \end{matrix}
\end{equation}


\linehorizon

4. Assume the above problem feasible and bounded, and let $[x^\star, y^\star]$ be its optimal solution. Using the strong duality property of linear programs, show that:
\begin{itemize}
    \item the vector $[x^\star, y^\star]$ can also be obtained by solving \eqref{P} and \eqref{D},
    \item the optimal value of \eqref{self-dual} is exactly 0.
\end{itemize}

This problem can be broken in the two following pieces \eqref{t1}, \eqref{t2}.

\begin{equation}
    \label{t1}
    \begin{matrix}
        & \min_{x, y} c^\top x \\
        \text{s.t} & A x = b \\
        & x \geq 0 \\
        & A^\top y \leq c
    \end{matrix}
\end{equation}

\begin{equation}
    \label{t2}
    \begin{matrix}
        & \max_{x, y} b^\top y \\
        \text{s.t} & A x = b \\
        & x \geq 0 \\
        & A^\top y \leq c
    \end{matrix}
\end{equation}

by removing the unneccessary constraints on both problems, this is equivalent to solving \eqref{P} and \eqref{D}.

\todo[inline]{Show that the optimal value of \eqref{self-dual} is exactly 0}

\section*{Exercise 2: Regularized Least-Square}

For given $A \in \mathbb{R}^{n \times d}$ and $b \in \mathbb{R}^n$, consider the following optimization problem :

\begin{equation}
    \label{machin}
    \min_x \Vert Ax - b \Vert_2^2 + \Vert x \Vert_1
\end{equation}

1. Compute the conjugate of $\Vert x \Vert_1$.

\linehorizon

The conjugate $f^*$ is defined as:

$$
f^*(y) = \sup_{x \in \mathbb{R}}\ \{ y^\top x - \Vert x \Vert_1 \}
$$

Let's consider $p, q \in [1, \infty]$, such that $\frac{1}{p} + \frac{1}{q} = 1$. Let's suppose $\Vert y \Vert_p > 1$, then by definition of the dual norm, there is a $z$ such that $\Vert z \Vert_q \leq 1$ and
$y^\top z > 1$. We can set $x = uz$, we here have:

$$
y^\top x - \Vert x \Vert_q = u (y^\top z - \Vert z \Vert_1)
$$

that tends to $\infty$ when $t \to \infty$, and in this case $f^*(y) = \infty$.  Conversely, if $\Vert y \Vert_p \leq 1$, then we have $y^T x \leq \Vert x \Vert_q \Vert y \Vert_p$ for all $x$. Which implies for all $x$, $y^\top x - \Vert x \Vert_p \leq 0$. Therefore $x = 0$ is the value that maximizes $y^\top x - \Vert x \Vert_p$, and in this case $f^*(y) = 0$. In our case $(p,q) = (1, \infty)$:

$$
f^*(y) = \left\{
\begin{matrix}
    0 & \Vert y \Vert_\infty \leq 1 \\
    \infty & \text{otherwise}
\end{matrix}
\right.
$$

\linehorizon

\todo[inline]{2. Compute the dual of \eqref{machin}.}

\linehorizon


\section*{Exercise 3: Data Separation}

Assume we have $n$ data points $x_i \in \mathbb{R}^d$ , with label $y_i \in \{-1, 1\}$. We are searching for an hyper-plane defined by its normal $\omega$, which separates the points according to their label. Ideally, we would like to have:

$$
\omega^\top x_i \leq -1 \Rightarrow  y i = -1 \quad \text{and} \quad \omega^\top x_i \geq 1 \Rightarrow y_i = 1.
$$

Unfortunately, this condition is rarely met with real-life problems. Instead, we solve an optimization problem which minimizes the gap between the hyper-plane and the miss-classified points. To do so, we will use a specific \emph{loss function}


\begin{equation}
    \mathcal{L}(\omega, x_i, y_i) = \max \left\{ 0 ; 1 - y_i(\omega^\top x_i) \right\}
\end{equation}


which is equal to 0 when the point $x_i$ is well-classified (the sign of $\omega^\top x_i$ and $y_i$ is the same), but is strictly positive when the sign of $\omega^\top x_i$ and $y_i$ is different. To improve the performances, instead of minimizing the loss function alone, we also use a quadratic regularizer as follow,

\begin{equation}
    \label{sep1}
    \min_\omega \frac{1}{n} \sum_{i=1}^n \mathcal{L}(\omega, x_i, y_i) + \frac{\tau}{2} \Vert \omega \Vert_2^2
\end{equation}

where $\tau$ is the regularization parameter.

1. Consider the following quadratic optimization problem (1 is a vector full of ones),

\begin{equation}
    \label{sep2}
    \begin{matrix}
        &\min_{\omega, z} \frac{1}{n\tau} \mathbf{1}^\top z + \frac{1}{2} \Vert \omega \Vert_2^2 \\
        \text{s.t} & z_i \geq 1 - y_i(\omega^\top x_i)\quad \forall i \in \{1, \cdots, n\} && (\lambda_i) \\
        & z \geq 0 & & (\pi)
    \end{matrix}
\end{equation}

Explain why problem \eqref{sep2} solves \eqref{sep1}.

\linehorizon

If we take the objective function of \eqref{sep2}, we have by rewritting it a bit by taking the constraint into consideration directly :

\begin{equation}
    \frac{1}{n\tau} \sum_{i=1}^n \max \left\{ 0 ; 1 - y_i(\omega^\top x_i) \right\} + \frac{1}{2} \Vert \omega \Vert_2^2
\end{equation}

and as $\tau > 0$ minimizing it — which is \eqref{sep2} — is exactly minimizing :

\begin{equation}
    \frac{1}{n} \sum_{i=1}^n \mathcal{L}(\omega, x_i, y_i) + \frac{\tau}{2} \Vert \omega \Vert_2^2
\end{equation}

which is \eqref{sep1}.

\linehorizon
2. Compute the dual of (Sep. 2), and try to reduce the number of variables. Use the notations $\lambda_i$
and $\pi$ for the dual variables.
\linehorizon

We have the following Lagrangian:

$$
\mathcal{L}(z,\omega, \lambda, \pi) = \frac{1}{n\tau}\mathbf{1}^\top z + \frac{1}{2} \Vert \omega \Vert_2^2 - \pi^\top z - \sum_{i=1}^n\lambda_i \left[1 - y_i (\omega^\top x_i) - y_i\right]
$$

$$
g(\lambda, \mu) = \inf_{z,\omega} \left\{\frac{1}{n\tau}\mathbf{1}^\top z + \frac{1}{2} \Vert \omega \Vert_2^2 - \pi^\top z - \sum_{i=1}^n\lambda_i \left[1 - y_i (\omega^\top x_i) - y_i\right] \right\}
$$

$\mathcal{L}$ is quadratic, hence differentiable and we have:

$$
\nabla_z \mathcal{L}(z,\omega, \lambda, \pi) = \frac{1}{n\tau}\mathbf{1} - \pi + \lambda
$$

$$
\nabla_\omega \mathcal{L}(z,\omega, \lambda, \pi) = \omega - \lambda \odot x \odot y
$$

That is at optimality :
$$
\frac{1}{\tau n}\mathbf{1} = \pi - \lambda
$$

$$
\omega = \lambda \odot x \odot y
$$

\todo{Simplify this.}

\section*{Exercise 4: Robust linear programming}

\todo[inline]{Optional}

\section*{Exercise 5: Boolean LP}

\todo[inline]{Optional}
